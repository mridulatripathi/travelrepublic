# Hotels

The result set can be filtered by:

* Name
* Stars
* UserRating
* MinCost

and sort the data by:

* Distance
* Stars
* MinCost
* UserRating


Data is displayed with the help of infinite scroll. All actions happen on client side.
 
 Frameworks used:

* [angularjs] 
* [Express]
* [Grunt]
* [less]
* [jQuery] 


###  After cloning the repo, follow the steps:
```sh
$ npm install
$ bower install
grunt
node app
```

Access the app at http://localhost:3000