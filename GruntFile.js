module.exports = function(grunt) {

    grunt.initConfig({
        less: {
            src: {
                // no need for files, the config below should work
                expand: true,
                cwd:    "app/css",
                src:    "*.less",
                dest:   "app/css/",
                ext:    ".css"
            }/*
            options: {
                paths: ["app/css"]
            },
            files: {
                "app/css/app.css": "app/css/app.less"
            }*/
        }
    });
    grunt.loadNpmTasks('grunt-contrib-less');

    grunt.registerTask('default', ['less']);
};