'use strict';
tr.service('dataService', function($http, $q, $rootScope) {
    this.partialData = [];
    var i = 0;
    this.getData = function(pageSize) {
        this.pageNo = 1;
        this.pageSize = pageSize;
        var THIS = this;
        if(this.partialData.length === 0 && !this.data) {
            var deferred = $q.defer();
            $http.get("app/data/hotels.json").success(function(response) {
                THIS.partialData = response.Establishments.slice(1, pageSize + 1);
                THIS.data = response.Establishments;
                deferred.resolve({data : THIS.partialData, total: THIS.data.length});
                THIS.originalData = THIS.data;
            }).error(function(error) {
                deferred.reject(error);
            });
            return deferred.promise;
        } else {

            return lazyLoadData(this.partialData, this.data, pageSize);
        }
    };
    var lazyLoadData = function(partialResult, result, pageSize) {
        var deferred = $q.defer();
        if(partialResult.length <= result.length) {
            this.pageNo = ++i;
            this.partialData = result.length < 50? result : result.slice(1, this.pageNo*pageSize + 1);
            deferred.resolve({data : this.partialData, total: result.length});
        } else {
            $rootScope.$emit("loadedAllData");
            deferred.resolve([]);
        }
        return deferred.promise;
    }.bind(this);
    this.getSortedData = function(order) {
        /*sort all data*/
        var sortOrder = order[order.name] ? 1 : -1;
        this.data.sort(function(x, y){
            if (x[order.name] < y[order.name]) {
                return -1*sortOrder;
            }
            if (x[order.name] > y[order.name]) {
                return sortOrder;
            }
            return 0;
        });
        /*return only this.pageSize*/
        this.partialData = (this.data.length < 50) ? this.data : this.data.slice(1, this.pageNo*this.pageSize + 1);
        return {data : this.partialData, total: this.data.length};
    };

    this.getSearchResults = function(query, filter) {
        /*get all data that satisfies query*/
        this.data =  this.originalData.filter(function(hotel) {
            if(query) {
                if(filter !== "MinCost") {
                    return (hotel[filter].toString().indexOf(query) > -1);
                } else {
                    return (hotel[filter] >= query);
                }
            } else {
                return true;
            }
        });
        /*return only this.pageSize*/
        this.partialData = (this.data.length < 50) ? this.data : this.data.slice(1, this.pageNo*this.pageSize + 1);
        return {data : this.partialData, total: this.data.length};
    };
    return this;
});