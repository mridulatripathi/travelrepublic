'use strict';
tr.directive('hotelsHolder', function($rootScope ,dataService) {
        return {
            restrict: 'E',
            templateUrl: "app/directives/hotelsHolder/hotelsHolder.html",
            controller: function ($scope) {
                $scope.pageSize = 50;
                $scope.hotels = [];
                $scope.excludedKeys = ["EstablishmentId", "EstablishmentType", "ImageUrl","ThumbnailUrl"];
                $rootScope.$on("loadedAllData", function() {
                    $scope.loadedAllData = true;
                });
                $scope.loadData = function() {
                    dataService.getData($scope.pageSize).then(function(response) {
                        if(!$scope.loadedAllData) {
                            $scope.hotels = response.data;
                            $scope.total = response.total;
                            if(!$scope.keys) {
                                $scope.keys = Object.keys($scope.hotels[0]);
                                $scope.excludedKeys.forEach(function(key) {
                                    $scope.keys.splice($scope.keys.indexOf(key), 1);
                                });
                            }
                        }
                    });
                };
                $scope.loadData();
                $scope.getSearchResults = function() {
                    var response = dataService.getSearchResults($scope.query, $scope.filter);
                    $scope.hotels = response.data;
                    $scope.total = response.total;
                };
                $scope.filters = ["Name", "Stars", "UserRating", "MinCost"];
                $scope.sortFilters = ["Distance", "Stars", "UserRating", "MinCost"];
                $scope.filter = "Name";
                $scope.order = {};
                $scope.setOrder = function (order) {
                    if($scope.sortFilters.indexOf(order) > -1) {
                        $scope.order.name = order;
                        $scope.orderKey = order;
                        $scope.order[order] = !$scope.order[order];
                        var response = dataService.getSortedData($scope.order);
                        $scope.hotels = response.data;
                        $scope.sortBasis = order;
                        $scope.total = response.total;
                    }
                };
            }
        }
    });