'use strict';
tr.directive('scrollInfinite', function() {
    return {
        restrict: 'A',
        link: function ($scope, $element, attrs) {
            var visibleHeight = $element.height();
            var threshold = 100;

            $element.scroll(function() {
                var scrollableHeight = $element.prop('scrollHeight');
                var hiddenContentHeight = scrollableHeight - visibleHeight;

                if (hiddenContentHeight - $element.scrollTop() <= threshold) {
                    $scope.$apply(attrs.scrollInfinite);
                }
            });
        }
    }
});